// [SECTION] While Loop

// if the condition evaluates to true, the statements inside the code block will executed


/*
	Syntax :
	
	while(expression/condition){
		statement
	}
*/



// While the value of count is not equal to 0
/*let count = 0;

while (count !== 5){
	console.log("while: " + count);

	
	// iteration it increases the value of the count after evevry iteration to stop the loop when it reaches 5


	// "++" increment, "--" decrement

	count++;


}
*/
// [SECTION] Do While Loop


// A do-while loop works a lot like the while loop. But unlike while loops, do-while loops guarantee that the code will executed at least once.

/*
	Syntax:
	do {
		statement
	}
	while (expression/condition)
	
	
*/


let number = Number(prompt("Give me a Number: "));
do{
	console.log("Do while: " + number);

	number += 1;
}while(number < 10);

// [SECTION] for loops

// a for loop is more flexible than while and do while loops. 

/*
	Syntax 
	for (initializaztion; expression/condition; finalExpression){
	
		statement
	}
*/



for (let count = 0; count <= 20; count++){
	console.log(count);
}



// let myString = prompt("give me a word: ")


// Character in strings may be counted using the .length property


// console.log(myString.length);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);

// Will create aloop that will print out the individual letters of a variable

for (let x = 0; x < myString.length; + x++){

	// The curernt value of my string is printed out using its index value


	console.log(myString[x]);
}


// Changing of vowels using loops

let myName = "DelIZo";

for (let i = 0; i < myName.length; i++){
	if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "u" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "o" 
	){
		console.log(3);
	} else {
		console.log(myName[i]);
	}
}